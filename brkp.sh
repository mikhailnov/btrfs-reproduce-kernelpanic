#!/bin/bash

# btrfs-reproduce-kernelpanic - brkp.sh
# This script _tries_ to reproduce random kernel panics 'Unable to mount UUID=xxx, VFS not syncing on unknown blok 0;0' while booting BTRFS with GRUB2
# Author: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>, 2018
# License: GPLv3

# currently try to support Ubuntu and ALT Linux distributions

if [ "$(id -u)" != "0" ]; then
	echo "Run this script as root!"
	exit 1
fi

echo_help(){
	echo "
Run this script with one of parameters:
	./brkp.sh run
		run this script
	./brkp.sh install
		install the script and rhe cron file for @reboot
	./brkp.sh uninstall
		uninstal what is installed
	"
}

disk_io_gen(){
	rm -fv /var/brkp-urandom
	dd if=/dev/urandom of=/var/brkp-urandom status=progress &
	dd_PID="$(echo $!)"
	echo "${dd_PID}" | tee /tmp/brkp-urandom.pid
}

initramfs_rebuild_ubuntu(){
	echo ""
	last_initrd_file="$(/bin/ls -1v /boot | grep ^initrd.img | tail -n 1)"
	rm -fv "$last_initrd_file"
	last_initrd_version="$(echo "$last_initrd_file" | awk -F "initrd.img-" '{print $2}')"
	update-initramfs -c -k "$last_initrd_version"
}

run(){
	if [[ ! -z $(cat /etc/*release | grep -i ubuntu) ]]
		then distro="ubuntu"
	elif [[ ! -z $(cat /etc/*release | grep ALT) ]]
		then distro="ALT"
	fi

	case "$distro" in
	"ubuntu")
		disk_io_gen &
		initramfs_rebuild_ubuntu
		sleep 2
		kill -9 "$(cat /tmp/brkp-urandom.pid)"
		reboot
	;;
	"ALT")
		echo "ALT Linux support is not implemented yet"
		exit 1
	;;
	*)
		echo "Your Linux distribution is unknown and is not supported"
		exit 1
	;;
	esac
}

######################################################################################
case "$1" in
	"run")
		run "$@"
	;;
	"uninstall")
		rm -fv /usr/local/bin/brkp /etc/cron.d/brkp
	;;
	"install")
		scriptpath="$( cd "$(dirname "$0")" ; pwd -P )"
		cp -v "${scriptpath}/brkp.sh" /usr/local/bin/brkp
		chmod +x /usr/local/bin/brkp
		cp -v "${scriptpath}/brkp-cron" /etc/cron.d/brkp-cron
	;;
	*)
		echo_help
	;;
esac
